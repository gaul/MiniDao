package com.gaul.minidao.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

/**
 * DAO层泛型基类接口
 */
public interface IGenericBaseCommonDao {

	public <T> void delete(T entitie);

	/**
	 * 根据主键删除指定的实体
	 */
	public <T> void deleteEntityById(Class<?> entityName, Serializable id);

	public <T> T findUniqueByProperty(Class<T> entityClass,
			String propertyName, Object value);

	public <T> T get(Class<T> entityClass, final Serializable id);

	public <T> T get(T entitie);

	public Session getSession();

	public <T> List<T> loadAll(T entitie);

	public <T> void save(T entity);

	public <T> void saveOrUpdate(T entity);
}
