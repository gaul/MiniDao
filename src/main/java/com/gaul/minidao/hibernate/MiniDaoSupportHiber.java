package com.gaul.minidao.hibernate;

import java.io.Serializable;
import java.util.List;

/**
 * 支持Hbiernate实体维护 MiniDao自动生成SQL
 */
@SuppressWarnings("hiding")
public interface MiniDaoSupportHiber<T> {
	/**
	 * 删除对象
	 */
	void deleteByHiber(T entity);

	/**
	 * 根据主键删除指定的实体
	 */
	public <T> void deleteByIdHiber(Class<?> entityName, Serializable id);

	/**
	 * 获取的对象
	 */
	<T> T getByEntityHiber(T entity);

	/**
	 * 获得对象
	 */
	<T> T getByIdHiber(Class<T> entityClass, final Serializable id);

	/**
	 * 查询对象列表
	 */
	List<T> listByHiber(T entity);

	/**
	 * 保存实体
	 */
	void saveByHiber(T entity);

	/**
	 * 更新对象
	 */
	void updateByHiber(T entity);
}
